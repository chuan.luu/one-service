package com.example.oneservice.service;

import com.example.oneservice.model.response.EmployeeDto;

/**
 * @author chuanlh on 2021-05-26
 */
public interface EmployeeService {

  EmployeeDto getEmployee();
}
