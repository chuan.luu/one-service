package com.example.oneservice.service.impl;

import com.example.oneservice.model.response.EmployeeDto;
import com.example.oneservice.service.EmployeeService;
import org.springframework.stereotype.Service;

/**
 * @author chuanlh on 2021-05-26
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

  @Override
  public EmployeeDto getEmployee() {
    return new EmployeeDto();
  }
}
