package com.example.oneservice.controller;

import com.example.oneservice.model.response.EmployeeDto;
import com.example.oneservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chuanlh on 2021-05-26
 */
@RequestMapping("employee")
@RestController
public class EmployeeController {

  @Autowired
  EmployeeService employeeService;

  @GetMapping
  public ResponseEntity<EmployeeDto> getEmmloyee() {
    return ResponseEntity.ok(employeeService.getEmployee());
  }
}
