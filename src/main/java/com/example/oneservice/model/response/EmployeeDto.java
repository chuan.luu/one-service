package com.example.oneservice.model.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @author chuanlh on 2021-05-26
 */
@Getter
@Setter
public class EmployeeDto {

  private Long id = 1L;
  private String name = "Hello";
  private String address = "World";
}
